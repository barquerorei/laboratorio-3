/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JuegoDeTerror;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class Nivel3 extends javax.swing.JFrame {

      public Timer Cronometro;
      int cont = 30;
      int INTENTOS = 0;
      int Random = 0;
      
      
    public Nivel3() {
        initComponents();
        
        this.setSize(455, 432);
        this.setLocationRelativeTo(null);
        timer.start();
        Random = (int)(Math.random()*40);
        txtrespuesta.setText("COMIENZA EL JUEGO :)");
    }

    Timer timer = new Timer(1000, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            lblCronometro.setText(Integer.toString(cont));
            cont--;
            if (cont == 0) {
                dispose();
          
            }
        }
     });
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblCronometro = new javax.swing.JLabel();
        btnjuegonuevo = new javax.swing.JButton();
        txtNum = new javax.swing.JTextField();
        txtrespuesta = new javax.swing.JTextField();
        lblleyenda2 = new javax.swing.JLabel();
        lblleyenda3 = new javax.swing.JLabel();
        lblleyenda = new javax.swing.JLabel();
        btnJugar = new javax.swing.JButton();
        FONDO = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        btnAyuda = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblCronometro.setFont(new java.awt.Font("Broadway", 1, 36)); // NOI18N
        lblCronometro.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.add(lblCronometro, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 10, 70, 50));

        btnjuegonuevo.setText("REINICIAR JUEGO");
        btnjuegonuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnjuegonuevoActionPerformed(evt);
            }
        });
        jPanel1.add(btnjuegonuevo, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 100, 120, -1));
        jPanel1.add(txtNum, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, 100, 30));
        jPanel1.add(txtrespuesta, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 160, 390, 130));

        lblleyenda2.setFont(new java.awt.Font("Broadway", 1, 12)); // NOI18N
        lblleyenda2.setForeground(new java.awt.Color(255, 255, 255));
        lblleyenda2.setText("Adivina el numero ");
        jPanel1.add(lblleyenda2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 140, 40));

        lblleyenda3.setFont(new java.awt.Font("Broadway", 1, 12)); // NOI18N
        lblleyenda3.setForeground(new java.awt.Color(255, 255, 255));
        lblleyenda3.setText("AQUÍ");
        jPanel1.add(lblleyenda3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 70, 60, -1));

        lblleyenda.setFont(new java.awt.Font("Broadway", 1, 14)); // NOI18N
        lblleyenda.setForeground(new java.awt.Color(255, 255, 255));
        lblleyenda.setText("CORRE QUE EL TIEMPO SE AGOTA...");
        jPanel1.add(lblleyenda, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 300, 40));

        btnJugar.setText("JUGAR");
        btnJugar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnJugarActionPerformed(evt);
            }
        });
        jPanel1.add(btnJugar, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 100, 120, -1));

        FONDO.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Pasillo 1.jpg"))); // NOI18N
        jPanel1.add(FONDO, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 440, 370));

        jMenuBar1.setBackground(new java.awt.Color(102, 0, 0));
        jMenuBar1.setBorderPainted(false);
        jMenuBar1.setFont(new java.awt.Font("Segoe UI Black", 0, 12)); // NOI18N

        btnAyuda.setText("Ayuda");
        btnAyuda.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnAyudaMouseClicked(evt);
            }
        });
        jMenuBar1.add(btnAyuda);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnJugarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnJugarActionPerformed
        try{
        
          int num = Integer.parseInt(txtNum.getText());//Solamente numeros enteros
          
          if (num == Random){
              JOptionPane.showMessageDialog(null, "Exelente has teminado con exito");
              
            txtrespuesta.setText("¡¡¡ CORRECTO  !!!");
            txtrespuesta.setText("Acertaste en:"+INTENTOS+ " "+"intentos");
            btnJugar.setEnabled(false);
            btnjuegonuevo.setEnabled(true);
            timer.stop();

          }//Fin del if
          
          else if (num < Random)
              txtrespuesta.setText(num+" " +"El numero es menor");
            else
             txtrespuesta.setText(num+ " " +"El numero es mayor");
          
          txtNum.setText("");
          txtNum.requestFocus();
          INTENTOS++;
          

        }catch(NumberFormatException ex){
         JOptionPane.showMessageDialog(null, ex.getMessage());
         
        }

    }//GEN-LAST:event_btnJugarActionPerformed

    private void btnAyudaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAyudaMouseClicked
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null, "Adivina el número del 0 al "
                + "40 en 30 segundos");
    }//GEN-LAST:event_btnAyudaMouseClicked

    private void btnjuegonuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnjuegonuevoActionPerformed
        
        btnJugar.setEnabled(true);
         btnjuegonuevo.setEnabled(false);
         INTENTOS = 0;
         Random = (int)(Math.random()*40);
         txtrespuesta.setText("");
         txtNum.requestFocus();
         txtrespuesta.setText("COMIENZA EL JUEGO :)");
         timer.start();
    }//GEN-LAST:event_btnjuegonuevoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Nivel3.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Nivel3.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Nivel3.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Nivel3.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Nivel3().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel FONDO;
    private javax.swing.JMenu btnAyuda;
    private javax.swing.JButton btnJugar;
    private javax.swing.JButton btnjuegonuevo;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblCronometro;
    private javax.swing.JLabel lblleyenda;
    private javax.swing.JLabel lblleyenda2;
    private javax.swing.JLabel lblleyenda3;
    private javax.swing.JTextField txtNum;
    private javax.swing.JTextField txtrespuesta;
    // End of variables declaration//GEN-END:variables
}
